/*
Copyright © 2021 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/skoved/deltr/twitter"
)

var validArgs = []string{"twitter"}

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Login to twitter.com",
	Long: `
Login to twitter.com
This command opens the default browser to the Twitter to give permissions
to deltr to read and delete posts on your behalf.`,
	ValidArgs: validArgs,
	Args:      cobra.ExactArgs(len(validArgs)),
	Run:       loginRun,
}

func loginRun(cmd *cobra.Command, args []string) {
	twt, err := twitter.New()
	if err != nil {
		log.Printf("%s\n", err)
	}
	log.Printf("Created twitter obj %+v\n", twt)

	if twt.Token == nil {
		err := twt.Login()
		cobra.CheckErr(err)
		log.Printf("Logged in to %s, successfully!\n", args[0])
		_, err = twt.Me()
		cobra.CheckErr(err)
		err = twt.Save()
		cobra.CheckErr(err)
	} else {
		log.Printf("Already logged into %s.\n", args[0])
	}
}

func init() {
	rootCmd.AddCommand(loginCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// loginCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// loginCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
