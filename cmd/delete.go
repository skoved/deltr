/*
Copyright © 2022 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/skoved/deltr/twitter"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete tweets",
	Long: `Delete tweets. Accepts tweet ids as args.
Ex: deltr 123456 234567 ... 67890
Must be logged in to twitter first. Check "deltr login --help" for more info.`,
	Args: cobra.MinimumNArgs(1),
	Run:  deleteRun,
}

func deleteRun(cmd *cobra.Command, args []string) {
	twt, err := twitter.New()
	if err != nil || twt.Token == nil {
		fmt.Fprintf(os.Stderr, "To delete posts you must be logged into twitter.\nYou can login by running: deltr login twitter")
		os.Exit(1)
	}
	if twt.UserID == "" {
		_, err = twt.Me()
		cobra.CheckErr(err)
	}

	for _, postID := range args {
		err := twt.DeletePost(postID)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not delete post: %s\n", postID)
		} else {
			fmt.Fprintf(os.Stderr, "Deleted post: %s\n", postID)
		}
	}
}

func init() {
	rootCmd.AddCommand(deleteCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
