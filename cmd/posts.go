/*
Copyright © 2021 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/skoved/deltr/twitter"
)

// postsCmd represents the posts command
var postsCmd = &cobra.Command{
	Use:   "posts",
	Short: "List posts from a user on a specified social media website",
	Long:  `List posts from a specified user on a specified social media website. Supported websites: twitter`,
	Args:  cobra.NoArgs,
	Run:   postsRun,
}

var (
	pageSize int
	output   string
	next     bool
	prev     bool
)

func isValidOutputType(output string) bool {
	outputTypes := []string{"plain", "table", "json"}
	for _, otype := range outputTypes {
		if otype == output {
			return true
		}
	}
	return false
}

func postsRun(cmd *cobra.Command, args []string) {
	if pageSize < 5 || pageSize > 100 {
		fmt.Fprintf(os.Stderr, "page-size must be >= 5 and <= 100.\n")
		os.Exit(1)
	}
	if !isValidOutputType(output) {
		fmt.Fprintf(os.Stderr, "%s is an invalid option for output. Accepted values for output are table, json, and plain. The default is table.\n", output)
		os.Exit(1)
	}
	if prev && next {
		fmt.Fprintf(os.Stderr, "The --next and --prev flags cannot be used at the same time.\n")
		os.Exit(1)
	}

	twt, err := twitter.New()
	if err != nil || twt.Token == nil {
		fmt.Fprintf(os.Stderr, "To get posts you must be logged into twitter.\nYou can login by running: deltr login twitter")
		os.Exit(1)
	}
	if twt.UserID == "" {
		_, err = twt.Me()
		cobra.CheckErr(err)
	}

	posts, err := twt.Posts(pageSize, next, prev)
	cobra.CheckErr(err)

	switch output {
	case "table":
		table := tablewriter.NewWriter(os.Stdout)
		table.SetRowLine(true)
		table.SetHeader([]string{"ID", "Text"})
		for _, post := range posts {
			table.Append([]string{post.Id, post.Text})
		}
		table.Render()
	case "json":
		payload := make(map[string][]twitter.Post)
		payload["posts"] = posts
		data, err := json.Marshal(payload)
		cobra.CheckErr(err)
		fmt.Println(string(data))
	case "plain":
		writer := csv.NewWriter(os.Stdout)
		writer.Comma = '\t'
		for _, post := range posts {
			writer.Write([]string{post.Id, post.Text})
		}
		writer.Flush()
		cobra.CheckErr(writer.Error())
	}

	twt.Save()
}

func init() {
	rootCmd.AddCommand(postsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// postsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// postsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	postsCmd.Flags().IntVar(&pageSize, "page-size", 5, "Set the number of tweets printed. This number must be >= 5 and <= 100.")
	postsCmd.Flags().StringVarP(&output, "output", "o", "table", "Sets the output format. Options are: table, plain, json.")
	postsCmd.Flags().BoolVarP(&next, "next", "n", false, "Gets the next page of posts. The requires having successfully run: deltr posts")
	postsCmd.Flags().BoolVarP(&prev, "prev", "p", false, "Gets the previous page of posts. This requires having successfully run: deltr posts --next. See the next flag for more info.")
}
