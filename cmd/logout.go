/*
Copyright © 2022 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/skoved/deltr/twitter"
)

// logoutCmd represents the logout command
var logoutCmd = &cobra.Command{
	Use:   "logout",
	Short: "Logout of twitter.com",
	Long: `Logout of twitter.com
Invalidates access and refresh tokens for twitter via the API. To revoke all
permissions for deltr you can go to https://twitter.com/settings/connected_apps`,
	Args: cobra.NoArgs,
	Run:  logoutRun,
}

func logoutRun(cmd *cobra.Command, args []string) {
	twt, err := twitter.New()
	if err != nil || twt.Token == nil {
		cobra.CheckErr(fmt.Errorf("Deltr is already logged out of twitter."))
	}

	err = twt.Logout()
	if err != nil {
		cobra.CheckErr(err)
	}
	err = twt.Save()
	if err != nil {
		cobra.CheckErr(err)
	}
	fmt.Fprintf(os.Stderr, "You have successfully logged out!\nTo fully revoke deltr's access to your twitter, you can go to https://twitter.com/settings/connected_apps\n")
}

func init() {
	rootCmd.AddCommand(logoutCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// logoutCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// logoutCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
