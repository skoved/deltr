package args

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var supportedSites = []string{"twitter"}

func ValidateArgs(cmd *cobra.Command, args []string) error {
	if len(args) < 2 {
		return fmt.Errorf("requires a website and username. See deltr posts -h for help.\n")
	}

	for _, site := range supportedSites {
		if strings.EqualFold(args[0], site) {
			return nil
		}
	}
	return fmt.Errorf("website %s, is not supported. See deltr posts -h for help.\n", args[0])
}
