module gitlab.com/skoved/deltr

go 1.16

require (
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/grokify/go-pkce v0.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/zalando/go-keyring v0.1.1 // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
)
