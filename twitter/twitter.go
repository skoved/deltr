/*
Copyright © 2021 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package twitter

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	nurl "net/url"
	"strconv"
	"strings"
	"time"

	"github.com/grokify/go-pkce"
	"github.com/pkg/browser"
	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"
	"gitlab.com/skoved/deltr/callback"
	"gitlab.com/skoved/deltr/utils"
	"golang.org/x/oauth2"
)

type Twitter struct {
	Token     *oauth2.Token `json:"-"`
	LoggedIn  bool
	User      string
	UserID    string
	NextPosts string
	PrevPosts string
	client    *http.Client `json:"-"`
}

type User struct {
	Id       string `json:"id,omitempty"`
	Username string `json:"username,omitempty"`
	Detail   string `json:"detail,omitempty"`
}

type PostResponse struct {
	Posts []Post        `json:"data"`
	Meta  *PostMetadata `json:"meta"`
}

type Post struct {
	Id   string `json:"id"`
	Text string `json:"text"`
}

type PostMetadata struct {
	OldestID  string `json:"oldest_id"`
	NewestID  string `json:"newest_id"`
	NumTweets int    `json:"result_count"`
	NextPage  string `json:"next_token,omitempty"`
	PrevPage  string `json:"previous_token,omitempty"`
}

type TweetDeleteResp struct {
	Data map[string]bool `json:"data"`
}

func (resp *TweetDeleteResp) NotDeleted() bool {
	deleted, exists := resp.Data["deleted"]
	return !(exists && deleted)
}

func New() (Twitter, error) {
	filename := viper.GetString("datafile")
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("There was an error reading %s\n%v\n", filename, err)
		return Twitter{}, nil
	}

	var twitter Twitter
	if err := json.Unmarshal(b, &twitter); err != nil {
		return Twitter{}, err
	}
	if twitter.LoggedIn {
		secret, err := keyring.Get(viper.GetString("twitter.service"), twitter.User)
		if err != nil {
			return Twitter{}, err
		}
		var token oauth2.Token
		err = json.Unmarshal([]byte(secret), &token)
		if err != nil {
			return Twitter{}, err
		}
		twitter.Token = &token
		config := getOauthConfig()
		twitter.client = config.Client(context.Background(), twitter.Token)
	}

	return twitter, nil
}

func (twitter *Twitter) Save() error {
	// The http.Client that is returned by oauth2.Config.Client() uses an
	// oauth2.ReuseTokenSource to provide a valid AccessToken. This is stored in
	// oauth2.Transport.Source. To access oauth2.Transport.Source it must be cast
	// from http.RoundTripper
	transport, ok := twitter.client.Transport.(*oauth2.Transport)
	if !ok {
		return fmt.Errorf("Could not get TokenSource from the twitter http client.")
	}
	token, err := transport.Source.Token()
	if err != nil {
		return err
	}
	twitter.Token = token

	tokenBytes, err := json.Marshal(twitter.Token)
	if err != nil {
		return err
	}
	err = keyring.Set(viper.GetString("twitter.service"), twitter.User, string(tokenBytes))
	if err != nil {
		return err
	}

	datafile := viper.GetString("datafile")
	file, err := json.MarshalIndent(twitter, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(datafile, file, 0644)
	return err
}

func (twitter *Twitter) Login() error {
	conf := getOauthConfig()

	state, err := utils.GenerateRandomString(32)
	if err != nil {
		return err
	}
	codeVerifier := pkce.NewCodeVerifier()
	codeChallenge := pkce.CodeChallengeS256(codeVerifier)

	twitterUrl := conf.AuthCodeURL(state, oauth2.SetAuthURLParam(pkce.ParamCodeChallenge, codeChallenge), oauth2.SetAuthURLParam(pkce.ParamCodeChallengeMethod, pkce.MethodS256))

	log.Printf("Twitter login URL is %s\n", twitterUrl)
	log.Printf("You will be taken to your browser to authenticate")

	time.Sleep(1 * time.Second)

	browser.OpenURL(twitterUrl)
	server := callback.NewServer(conf, codeVerifier, state)
	tok, err := server.Run()
	if err != nil {
		return err
	}
	twitter.Token = tok
	twitter.LoggedIn = true
	twitter.client = conf.Client(context.Background(), twitter.Token)
	log.Printf("Twitter: %+v\n", twitter)

	return nil
}

func getOauthConfig() *oauth2.Config {
	clientId := viper.GetString("twitter.client_id")
	scopes := viper.GetStringSlice("twitter.scopes")

	authUrl := viper.GetString("twitter.api.auth")
	tokenUrl := viper.GetString("twitter.uri") + viper.GetString("twitter.api.token")

	return &oauth2.Config{
		ClientID:     clientId,
		ClientSecret: "",
		Scopes:       scopes,
		Endpoint: oauth2.Endpoint{
			AuthURL:   authUrl,
			TokenURL:  tokenUrl,
			AuthStyle: oauth2.AuthStyleInParams,
		},
		RedirectURL: viper.GetString("redirect"),
	}
}

func (twitter *Twitter) httpRequest(url string, httpMethod string, body *strings.Reader, headers *map[string]string, oauth bool) (*[]byte, error) {
	var (
		err    error
		req    *http.Request
		client *http.Client
	)
	if body != nil {
		req, err = http.NewRequest(httpMethod, url, body)
	} else {
		req, err = http.NewRequest(httpMethod, url, nil)
	}
	if err != nil {
		return nil, err
	}
	if oauth {
		client = twitter.client
	} else {
		client = &http.Client{}
	}
	if headers != nil {
		for key, value := range *headers {
			req.Header.Add(key, value)
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Error! received status code: %d from call to %s\n", resp.StatusCode, url)
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &bodyBytes, nil
}

func (twitter *Twitter) Me() (*User, error) {
	url := viper.GetString("twitter.uri") + viper.GetString("twitter.api.me")

	bodyBytes, err := twitter.httpRequest(url, http.MethodGet, nil, nil, true)
	if err != nil {
		return nil, err
	}

	var payload map[string]User
	err = json.Unmarshal(*bodyBytes, &payload)
	if err != nil {
		return nil, err
	}
	user, exists := payload["data"]
	if !exists {
		return nil, fmt.Errorf("%s\n", payload["errors"].Detail)
	}
	twitter.User = user.Username
	twitter.UserID = user.Id
	return &user, nil
}

func (twitter *Twitter) Posts(maxResults int, next bool, prev bool) ([]Post, error) {
	if twitter.UserID == "" {
		return []Post{}, errors.New("A twitter ID has not been saved yet.")
	}
	if next && twitter.NextPosts == "" {
		return []Post{}, errors.New("There is no next page token for twitter posts. To lookup twitter posts run: deltr posts")
	}
	if prev && twitter.PrevPosts == "" {
		return []Post{}, errors.New("There is no previous page token for twitter posts. To lookup twitter posts run: deltr posts")
	}

	postApi := fmt.Sprintf(viper.GetString("twitter.api.posts"), twitter.UserID)
	url := fmt.Sprintf("%s%s?exclude=retweets&max_results=%d", viper.GetString("twitter.uri"), postApi, maxResults)
	if next || prev {
		var pageToken string
		if next {
			pageToken = twitter.NextPosts
		} else {
			pageToken = twitter.PrevPosts
		}
		url += fmt.Sprintf("&pagination_token=%s", pageToken)
	}

	bodyBytes, err := twitter.httpRequest(url, http.MethodGet, nil, nil, true)
	if err != nil {
		return []Post{}, err
	}
	var payload PostResponse
	err = json.Unmarshal(*bodyBytes, &payload)
	if err != nil {
		return []Post{}, err
	}
	log.Printf("Post Response Metadata: %+v\n", payload.Meta)

	twitter.NextPosts = payload.Meta.NextPage
	twitter.PrevPosts = payload.Meta.PrevPage
	log.Printf("Twitter: %+v\n", twitter)
	return payload.Posts, nil
}

func (twitter *Twitter) DeletePost(postID string) error {
	url := viper.GetString("twitter.uri") + viper.GetString("twitter.api.delete.post")
	url = fmt.Sprintf(url, postID)

	bodyBytes, err := twitter.httpRequest(url, http.MethodDelete, nil, nil, true)
	if err != nil {
		return err
	}
	var payload TweetDeleteResp
	err = json.Unmarshal(*bodyBytes, &payload)
	if err != nil {
		return err
	}
	if payload.NotDeleted() {
		return fmt.Errorf("Twitter was unable to delete tweet with id: %s.\n", postID)
	}
	return nil
}

func (twitter *Twitter) revokeToken(clientID string, revokeUrl string, token string, tokenType string) error {
	if tokenType != "access_token" && tokenType != "refresh_token" {
		return fmt.Errorf("tokenType must be either access_token or refresh_token. It was %s.\n", tokenType)
	}
	data := nurl.Values{}
	data.Set("token", token)
	data.Set("client_id", clientID)
	data.Set("token_type_hint", tokenType)
	headers := &map[string]string{
		"Content-Type":   "application/x-www-form-urlencoded",
		"Content-Length": strconv.Itoa(len(data.Encode())),
	}
	body := strings.NewReader(data.Encode())

	bodyBytes, err := twitter.httpRequest(revokeUrl, http.MethodPost, body, headers, false)
	if err != nil {
		return err
	}
	var payload map[string]bool
	err = json.Unmarshal(*bodyBytes, &payload)
	if err != nil {
		return err
	}
	if !payload["revoked"] {
		return fmt.Errorf("Twitter did not accept %s.", tokenType)
	}
	return nil
}

func (twitter *Twitter) Logout() error {
	if twitter.Token == nil {
		return fmt.Errorf("Not logged in.")
	}
	url := viper.GetString("twitter.uri") + viper.GetString("twitter.api.revoke")
	clientID := viper.GetString("twitter.client_id")

	err := twitter.revokeToken(clientID, url, twitter.Token.RefreshToken, "refresh_token")
	if err != nil {
		log.Printf("failed to revoke refresh token\n")
		return err
	}
	err = twitter.revokeToken(clientID, url, twitter.Token.AccessToken, "access_token")
	if err != nil {
		log.Printf("failed to revoke access token\n")
		return err
	}
	err = keyring.Delete(viper.GetString("twitter.service"), twitter.User)
	if err != nil {
		log.Printf("failed to delete token in keyring")
		return err
	}
	twitter.Token = nil
	twitter.User = ""
	twitter.UserID = ""
	twitter.NextPosts = ""
	twitter.PrevPosts = ""
	twitter.LoggedIn = false
	return nil
}
