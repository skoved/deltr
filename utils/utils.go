/*
Copyright © 2021 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package utils

import (
	"crypto/rand"
	"encoding/base64"
)

func GenerateRandomBytes(i int) ([]byte, error) {
	b := make([]byte, i)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func GenerateRandomString(i int) (string, error) {
	b, err := GenerateRandomBytes(i)
	return base64.URLEncoding.EncodeToString(b), err
}
