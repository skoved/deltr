/*
Copyright © 2021 Sam Koved

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package callback

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/grokify/go-pkce"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

/*type ctSetterTransport struct {
	underlyingTransport http.RoundTripper
}

func (t *ctSetterTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// Twitter requires 'Content-Type: application/x-www-form-urlencoded' header
	// on the request exchangeing auth code for access token
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return t.underlyingTransport.RoundTrip(req)
}*/

type Server struct {
	Config       *oauth2.Config
	CodeVerifier string
	Port         string
	Route        string
	State        string
}

var (
	ctx    context.Context
	cancel context.CancelFunc
	token  *oauth2.Token
	user   string
	err    error
)

func (s *Server) Run() (*oauth2.Token, error) {
	ctx, cancel = context.WithCancel(context.Background())

	http.HandleFunc(s.Route, s.callbackHandler)

	srv := &http.Server{Addr: s.Port}
	go func() {
		srv_err := srv.ListenAndServe()
		if srv_err != http.ErrServerClosed {
			err = srv_err
		}
	}()

	<-ctx.Done()

	if err != nil {
		return nil, err
	}

	return token, nil
}

func (s *Server) callbackHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Received callback")
	queryParts, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		log.Println("Could not parse query parameters")
		cancel()
		return
	}

	var code, state string
	codeList, ok := queryParts["code"]
	if !ok {
		log.Println("Callback did not contain a 'code' query param")
		cancel()
		return
	}
	if len(codeList) < 1 {
		log.Println("Callback did contain a 'code' query param but there was no value assigned")
		cancel()
		return
	}
	code = codeList[0]
	stateList, ok := queryParts["state"]
	if !ok {
		log.Println("Callback did not contain a 'state' query param")
		cancel()
		return
	}
	if len(stateList) < 1 {
		log.Println("Callback did contain a 'state' query param but there was no value assigned")
		cancel()
		return
	}
	state = stateList[0]

	if s.State != state {
		log.Println("An invalid state was returned")
		err = errors.New("Invalid state returned from Twitter.")
		cancel()
		return
	}
	log.Println("Got a valid state from the response")
	log.Println("Making request for token")

	token, err = s.Config.Exchange(context.Background(), code, oauth2.SetAuthURLParam(pkce.ParamCodeVerifier, s.CodeVerifier))
	if err != nil {
		log.Println("An error occured exchanging a code for a token")
		cancel()
		return
	}

	log.Printf("Access token: %+v\n", token.AccessToken)
	// show success page
	msg := "<p><strong>Success</strong></p>"
	fmt.Fprintf(w, msg)

	cancel()
}

func NewServer(config *oauth2.Config, codeVerifier string, state string) *Server {
	port := viper.GetString("callback.port")
	route := viper.GetString("callback.route")

	return &Server{
		Config:       config,
		CodeVerifier: codeVerifier,
		Port:         port,
		Route:        route,
		State:        state,
	}
}
